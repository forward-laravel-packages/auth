<?php

namespace Forward\Auth;

use Illuminate\Support\Arr;
use Forward\Auth\Services\AuthUser;
use Illuminate\Support\ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
      // Publish The Configuration
      $this->publishes([
          __DIR__.'/config/forward-auth.php' => config_path('forward-auth.php')
      ],'forward-auth configuration');
      //Load the Routes file
      $this->loadRoutesFrom(__DIR__.'/routes.php');
      //Load the migrations directory
      $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
      $this->mergeConfigFrom(
          __DIR__.'/config/forward-auth.php', 'forward-auth'
      );
      $this->app->singleton('Forward\Auth\Contracts\AuthService', function(){
        return new AuthUser(
          $this->app['request']
        );
      });
    }

    /**
    * Get the services provided by the provider.
    * @return array
    */
     public function provides()
     {
         return ['Forward\Auth\Contracts\AuthService'];
     }
}
