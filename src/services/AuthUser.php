<?php

namespace Forward\Auth\Services;

use GuzzleHttp\Client;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use GuzzleHttp\ClientInterface;
use Illuminate\Http\RedirectResponse;
use Forward\Auth\Contracts\AuthService;
use Forward\Auth\User;

class AuthUser implements AuthService
{
    /**
     * The HTTP request instance.
     *
     * @var \Illuminate\Http\Request
     */
    protected $request;

    /**
     * The HTTP Client instance.
     *
     * @var \GuzzleHttp\Client
     */
    protected $httpClient;

    /**
     * The client ID.
     *
     * @var string
     */
    protected $clientId;

    /**
     * The client secret.
     *
     * @var string
     */
    protected $clientSecret;

    /**
     * The redirect URL.
     *
     * @var string
     */
    protected $redirectUrl;

    /**
     * The redirect URL.
     *
     * @var string
     */
    protected $grantType;

    /**
     * The redirect URL.
     *
     * @var string
     */
    protected $tokenType;

    /**
     * The custom parameters to be sent with the request.
     *
     * @var array
     */
    protected $parameters = [];

    /**
     * The scopes being requested.
     *
     * @var array
     */
    protected $scopes = ['email'];

    /**
     * The user fields being requested.
     *
     * @var array
     */
    protected $fields = [];

    /**
     * The separating character for the requested scopes.
     *
     * @var string
     */
    protected $scopeSeparator = ',';

    /**
     * The type of the encoding in the query.
     *
     * @var int Can be either PHP_QUERY_RFC3986 or PHP_QUERY_RFC1738.
     */
    protected $encodingType = PHP_QUERY_RFC1738;

    /**
     * The custom Guzzle configuration options.
     *
     * @var array
     */
    protected $guzzle = [];

    /**
     * The OAuth Token URL.
     *
     * @var string
     */
    protected $tokenUrl;

    public function __construct(Request $request){
        //Set The Request data
        $this->request = $request;
        $this->clientId = config('forward-auth.client.client_id');
        $this->clientSecret = config('forward-auth.client.client_secret');
        $this->redirectUrl = config('forward-auth.client.redirect_uri');
        $this->grantType = config('forward-auth.client.grant_type');
        $this->tokenType = config('forward-auth.client.token_type');
        $this->guzzle = config('forward-auth.guzzle');
    }

    /**
     * Redirect the user of the application to the provider's authentication screen.
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function redirect()
    {
        $query = http_build_query([
            'scope' => '*',
            'response_type' => 'code',
            'client_id' => $this->clientId,
            'redirect_uri' => $this->redirectUrl
         ]);
        return new RedirectResponse($this->getAuthUrl().'?'.$query);
    }

    /**
     * Get The User from the login code
     * @return \Forward\Auth\Models\User
     */
    public function user()
    {
        $response = $this->getAccessTokenResponse($this->getCode());
        $user = $this->mapUserToObject($this->getUserByToken(
            $token = Arr::get($response, 'access_token')
        ));

        return $user->setToken($token)
                    ->setRefreshToken(Arr::get($response, 'refresh_token'))
                    ->setExpiresIn(Arr::get($response, 'expires_in'));
    }

    /**
     * Register a user to the Auth API
     * @return \Forward\Auth\Models\User
     */
    public function register($user){

    }

    /**
     * Get a User instance from a known access token.
     *
     * @param  string  $token
     * @return \Forward\Auth\Models\User
     */
    public function userFromToken($token)
    {
        $user = $this->mapUserToObject($this->getUserByToken($token));
        return $user->setToken($token);
    }

    protected function getUserByToken($token)
    {
        $meUrl = config('forward-auth.client.host').'/api/me?&fields='.implode(',', $this->fields);

        $response = $this->getHttpClient()->get($meUrl, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => $this->tokenType.' '.$token
            ],
        ]);

        return json_decode($response->getBody(), true);
    }

    /**
     * {@inheritdoc}
     */
    protected function mapUserToObject(array $user)
    {
        return (new User)->setRaw($user);
    }
    /**
     * Get the access token response for the given code.
     *
     * @param  string  $code
     * @return array
     */
    public function getAccessTokenResponse($code)
    {
        $postKey = (version_compare(ClientInterface::VERSION, '6') === 1) ? 'form_params' : 'body';
        $response = $this->getHttpClient()->post($this->getTokenUrl(), [
            'headers' => ['Accept' => 'application/json'],
            $postKey => $this->getTokenFields($code),
        ]);
        return json_decode($response->getBody(), true);
    }

    /**
     * {@inheritdoc}
     */
    protected function getTokenUrl()
    {
        return config('forward-auth.client.host').'/oauth/token';
    }

    /**
     * {@inheritdoc}
     */
    protected function getAuthUrl()
    {
        return config('forward-auth.client.host').'/oauth/authorize';
    }

    /**
     * Get the POST fields for the token request.
     *
     * @param  string  $code
     * @return array
     */
    protected function getTokenFields($code)
    {
        return [
            'code' => $code,
            'client_id' => $this->clientId,
            'grant_type' => $this->grantType,
            'client_secret' => $this->clientSecret,
            'redirect_uri' => $this->redirectUrl
        ];
    }

    /**
     * Get the code from the request.
     *
     * @return string
     */
    protected function getCode()
    {
        return $this->request->input('code');
    }

    /**
     * Merge the scopes of the requested access.
     *
     * @param  array|string  $scopes
     * @return $this
     */
    public function scopes($scopes)
    {
        $this->scopes = array_unique(array_merge($this->scopes, (array) $scopes));
        return $this;
    }

    /**
     * Set the scopes of the requested access.
     *
     * @param  array|string  $scopes
     * @return $this
     */
    public function setScopes($scopes)
    {
        $this->scopes = array_unique((array) $scopes);
        return $this;
    }

    /**
     * Get the current scopes.
     *
     * @return array
     */
    public function getScopes()
    {
        return $this->scopes;
    }

    /**
     * Set the redirect URL.
     *
     * @param  string  $url
     * @return $this
     */
    public function redirectUrl($url)
    {
        $this->redirectUrl = $url;
        return $this;
    }

    /**
     * Get a instance of the Guzzle HTTP client.
     *
     * @return \GuzzleHttp\Client
     */
    protected function getHttpClient(){
        if (is_null($this->httpClient)) {
            $this->httpClient = new Client($this->guzzle);
        }
        return $this->httpClient;
    }

    /**
     * Set the Guzzle HTTP client instance.
     *
     * @param  \GuzzleHttp\Client  $client
     * @return $this
     */
    public function setHttpClient(Client $client)
    {
        $this->httpClient = $client;
        return $this;
    }

    /**
     * Set the request instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return $this
     */
    public function setRequest(Request $request)
    {
        $this->request = $request;

        return $this;
    }

    /**
     * Set the user fields to request from Facebook.
     *
     * @param  array  $fields
     * @return $this
     */
    public function fields(array $fields)
    {
        $this->fields = $fields;
        return $this;
    }

    /**
     * Set the user fields to request from Facebook.
     *
     * @param  array  $fields
     * @return $this
     */
    public function tokenType(array $tokenType)
    {
        $this->tokenType = $tokenType;
        return $this;
    }
}
