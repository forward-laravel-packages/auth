<?php

namespace Forward\Auth\Contracts;

Interface AuthService
{
    /**
     * Redirect the user to the authentication page.
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function redirect();

    /**
     * Get the User instance for the authenticated user.
     * @return \Forward\Auth\User
     */
    public function user();

    /**
     * Get the User instance from stored token
     * @return \Forward\User\Models\User
     */
    public function userFromToken($token);

    /**
     * Send a register petition to the authentication page.
     * @return Request
     */
    public function register($user);
}
