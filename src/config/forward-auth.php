<?php

return [
    /*
     |--------------------------------------------------------------------------
     | Forward Auth Connection
     |--------------------------------------------------------------------------
     |
     */
    'client' => [
        'host' => env('AUTH_API_HOST', 'http://authapi.forward.com.mx'),
        'client_id' => env('AUTH_API_CLIENT_ID', '1'),
        'client_secret' => env('AUTH_API_CLIENT_SECRET', ''),
        'grant_type' => env('AUTH_API_GRANT_TYPE', 'authorization_code'),
        'redirect_uri' => env('AUTH_API_REDIRECT_URI','http://localhost/callback'),
        'token_type' => env('AUTH_API_TOKEN_TYPE','Bearer'),
    ],
    'guzzle' => []
];
