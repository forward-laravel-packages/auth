<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUsersTableApiToken extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
          if(Schema::hasTable('users')){
              Schema::table('users', function (Blueprint $table) {
                $table->string('access_token')->nullable();
                $table->string('refresh_token')->nullable();
                $table->string('token_type')->nullable();
                $table->integer('expires_in')->nullable();
              });
          }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasTable('users')){
            Schema::table('users', function (Blueprint $table) {
              $table->dropColumn(['access_token', 'refresh_token', 'token_type', 'expires_in']);
            });
        }
    }
}
